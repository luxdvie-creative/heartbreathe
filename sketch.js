// Based on CodingTrain's work. Please watch his videos! He is an amazing human being.
// Heart Curve
// Daniel Shiffman
// https://thecodingtrain.com/CodingChallenges/134-heart-curve.html
// https://youtu.be/oUBAi9xQ2X4

let w;
let h;

let heart = [];
let a = 0;
let heartStrokeColor = 255;
let heartFillIndex = 0;
let heartFillColor = 0;

let setSize = function () {
	let el = $("#canvas-wrap");
	w = el.width();
	h = el.height();
};

let sketch = function (p) {
	window.p = p;
	setSize();

	let setColor = function () {
		heartFillIndex++;
		if (heartFillIndex > 360) {
			heartFillIndex = 0;
		}

		heartFillColor = p.color("hsl(" + heartFillIndex + ",80%,50%)");
	};

	p.setup = function () {
		p.createCanvas(w, h);
		p.background(0);
		setColor();
	};


	p.draw = function () {
		setColor();
		p.background(0);
		p.translate(w / 2, h / 2);
		p.stroke(heartStrokeColor);
		p.strokeWeight(2);
		p.fill(heartFillColor);

		p.beginShape();
		for (let v of heart) {
			p.vertex(v.x, v.y);
		}
		p.endShape();

		const r = h / 60;
		const x = r * 16 * p.pow(p.sin(a), 3);
		const y =
			-r *
			(13 * p.cos(a) -
				5 * p.cos(2 * a) -
				2 * p.cos(3 * a) -
				p.cos(4 * a));
		heart.push(p.createVector(x, y));

		// So that it stops
		if (a > 2 * p.TWO_PI) {
			heart = [];
			heartFillColor += 40;
			a = 0;
		}

		a += 0.1;
	};

	p.windowResized = function () {
		setSize();
		p.resizeCanvas(w, h);
	};
};

new p5(sketch, "canvas-wrap");
